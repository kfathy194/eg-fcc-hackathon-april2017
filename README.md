Hackathon April 2017
Building Task Manager Application a clone of Trello App.

We will be developing this application in two-way development front-end and back-end starting by,
A- Front-End Scenarios and Features:
1- The first page will be the login for existed user or signup for the new users to register their new accounts.
2- After user logged in will be forward to the dashboard.
3- The dashboard will present the latest boards or projects and tasks.
4- The board page, each user will be able to create a new board.
(Board is like a project having multiple teams working on it and every team has his own group of tasks.)
5- The list of the tasks: Every board has a column like that contains a group of the task index.
(Every list contains a group of tasks that related to the same domain)
6- The list capabilities: Each list is assigned to a certain team member, who has the ability to update the task state [started/finished/in-progress] The one who assigns this tasks to the users is the board owner.
7- Task details: Every task on the list has details that will be viewed in a box that shows by clicking on the task.
8- Front-End Tools: HTML/CSS/Angular2

B- Back-End Scenarios and Features:
Our main job is to provide APIs for the front-end developers,
1- APIs:
  1.1- Creating APIs for the register and login.
  1.2- Creating APIs for the boards, tasks, and task states [in progress or completed] these APIs will handling the user actions from the front-end to the back-end.

2- The login API security: We will use JWT authentication to generate user token.

3- Any other API this token will be used for checking user is Authenticated AND what is his role.

4- Register/Login feature can be using OAuth or traditional way (as you like)

5- Back-End Tools: ExpressJs/MongoDB and for managing mongo from express we will be using Mongoose.

NOTES:
1- Please make a visit to TRELLO https://goo.gl/6NPSEI  and do some digging to understand how it works, it will help you in the development time.

Thank You. :)


algorithm questions

https://github.com/kennymkchan/interview-questions-in-javascript
